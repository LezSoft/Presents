/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/default_snackbar.dart';
import 'package:presents/utils/default_text_form_field.dart';
import 'package:provider/provider.dart';

Future<void> showResetPasswordDialog(BuildContext context, {String? defaultEmail}) async {
  TextEditingController controller = TextEditingController(text: defaultEmail);

  ColorScheme themeColors = Theme.of(context).colorScheme;
  AuthService auth = Provider.of<AuthService>(context, listen: false);
  
  String? email = await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: const Text('Reset Your Password'),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text("Enter your email, if there is an account associated with "
              "it you will receive an email to reset your password",
            textAlign: TextAlign.justify,
          ),
          const SizedBox(height: 20.0,),
          DefaultTextFormField(
            controller: controller,
            icon: Icons.alternate_email,
            label: 'Email',
            validator: (value) {
              if (value == null || !value.contains('@') || value.contains(' ')) {
                return 'Please enter a valid email address';
              }
              else {
                return null;
              }
            }
          ),
        ],
      ),
      actionsPadding: const EdgeInsets.only(right: 15.0, bottom: 10.0),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(null),
          child: Text('CANCEL', style: TextStyle(
            fontWeight: FontWeight.bold,
            color: themeColors.onBackground,
          ),)
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(controller.text),
          child: Text('SEND', style: TextStyle(
            fontWeight: FontWeight.bold,
            color: themeColors.primary,
          ),)
        ),
      ],
    ),
  );

  if (email != null) {
    String? res = await auth.createPasswordRecovery(email: email);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ScaffoldMessenger.of(context).showSnackBar(
        (res == null)
            ? defaultSnackBar('Email sent, check your inbox!', error: false)
            : defaultSnackBar('Something went wrong:\n$res', error: true),
      );
    });
  }
}