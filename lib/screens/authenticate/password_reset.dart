/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/custom_button_style.dart';
import 'package:presents/utils/default_snackbar.dart';
import 'package:presents/utils/default_text_form_field.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:provider/provider.dart';

class PasswordReset extends StatefulWidget {
  const PasswordReset({
    Key? key,
    required this.userId,
    required this.secret
  }) : super(key: key);

  final String? userId;
  final String? secret;

  @override
  State<PasswordReset> createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {

  final TextEditingController _newPsw = TextEditingController();
  final TextEditingController _repeatNewPsw = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    if (widget.userId == null || widget.secret == null) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar('Invalid Link', error: true));
        await Future.delayed(const Duration(seconds: 6));

        if(!mounted) return;
        context.goNamed(routeName(RouteCodes.login));
      });
    }

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.orangeAccent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: 1,
                child: Container()
              ),
              Flexible(
                flex: 3,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text(
                            'Update your Presents password:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          const SizedBox(height: 25.0),

                          DefaultTextFormField(
                            controller: _newPsw,
                            icon: Icons.password,
                            label: 'New Password',
                            validator: (value) {
                              if (value == null || value.length < 8) {
                                return 'Passwords must be 8+ characters long';
                              }
                              else {
                                return null;
                              }
                            }
                          ),
                          const SizedBox(height: 20.0,),

                          DefaultTextFormField(
                            controller: _repeatNewPsw,
                            icon: Icons.password,
                            label: 'Repeat New Password',
                            validator: (value) {
                              if (value != _newPsw.text) {
                                return "Passwords don't match";
                              }
                              else {
                                return null;
                              }
                            },
                          ),
                          const SizedBox(height: 25.0,),

                          Consumer<AuthService>(
                            builder: (context, auth, child) {
                              return ElevatedButton(
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    String? res = await auth.resetPassword(
                                        widget.userId!,
                                        widget.secret!,
                                        _newPsw.text
                                    );

                                    if (!mounted) return;
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      (res == null) ? defaultSnackBar(
                                          "Password updated successfully!\n"
                                              "You'll be redirected to the login "
                                              "screen in few seconds",
                                          error: false)
                                          : defaultSnackBar(
                                          'Something went wrong:\n$res',
                                          error: true),
                                    );
                                    await Future.delayed(
                                        const Duration(seconds: 6));

                                    if (!mounted) return;
                                    context.goNamed(routeName(RouteCodes.login));
                                  }
                                },
                                style: customButtonStyle(),
                                child: Text('SAVE', style: TextStyle(
                                  color: Colors.grey[850])
                                ),
                              );
                            }
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container()
              ),
            ],
          ),
        ),
      ),
    );
  }
}
