/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/profile/profile_textarea_form_field.dart';
import 'package:presents/screens/profile/settings_dialog.dart';
import 'package:presents/screens/profile/show_profilepic_menu.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/default_appbar.dart';
import 'package:presents/screens/profile/profile_datetime_form_field.dart';
import 'package:presents/screens/profile/profile_text_form_field.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/navbar_vert.dart';
import 'package:presents/utils/random_colored_box.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

/// Current User's Profile Screen
class Profile extends StatelessWidget {
  const Profile({Key? key, required this.auth}) : super(key: key);

  final AuthService auth;

  Widget _showSettingsDialog(ColorScheme themeColors, DatabaseService db) {
    return AlertDialog(
      title: const Text('Settings:', textAlign: TextAlign.center,),
      backgroundColor: themeColors.background,
      content: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: SettingsDialog(auth: auth, db: db,),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    // This check is required to avoid an error immediately after pressing the LogOut button
    bool loggingOut = (auth.currUser == null);

    return loggingOut ? const Loading() : Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        //region APP BAR
        DefaultAppBar(title: 'Profile', actions: [
          Consumer<DatabaseService>(
            builder: (context, db, child) {
              return IconButton(
                icon: const Icon(Icons.settings),
                onPressed: () async {
                  await showDialog(context: context, builder: (context) => _showSettingsDialog(Theme.of(context).colorScheme, db));
                },
              );
            }
          ),
        ],),
        //endregion

        //region BODY
        Expanded(
            child: LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.maxWidth <= Constants.maxPortraitWidth) {
                  return PortraitProfile(auth: auth,);
                }
                else {
                  return LandscapeProfile(auth: auth,);
                }
              },
            )
        ),
        //endregion
      ],
    );
  }
}

class PortraitProfile extends StatefulWidget {
  const PortraitProfile({Key? key, required this.auth}) : super(key: key);

  final AuthService auth;

  @override
  State<PortraitProfile> createState() => _PortraitProfileState();
}
class _PortraitProfileState extends State<PortraitProfile> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late final TextEditingController _name;
  late final TextEditingController _username;
  DateTime? _birthday;
  late final TextEditingController _about;

  bool _isUsernameValid = true;

  @override
  void initState() {
    super.initState();
    _name = TextEditingController(text: widget.auth.currUser!.name);
    _username = TextEditingController(text: widget.auth.currUser!.username);
    _birthday = widget.auth.currUser!.birthday;
    _about = TextEditingController(text: widget.auth.currUser!.about);
  }

  @override
  Widget build(BuildContext context) {
    MyUser currUser = widget.auth.currUser!;

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Consumer<DatabaseService>(
              builder: (context, db, child) {
                return Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //region PROFILE PIC + NAME + USERNAME
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          //region Profile Pic
                          Consumer<DatabaseService>(
                              builder: (context, db, child) {
                                return GestureDetector(
                                  onTapUp: (details) async {
                                    await showProfilePicMenu(
                                      context: context,
                                      dbService: db,
                                      offset: details.globalPosition,
                                      currProfilePic: currUser.profilePic,
                                    );
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: Container(
                                      width: 100.0,
                                      height: 100.0,
                                      color: Theme.of(context).colorScheme.background,
                                      child: currUser.profilePic ?? const RandomColoredBox(),
                                    ),
                                  ),
                                );
                              }
                          ),
                          //endregion

                          const SizedBox(width: 25.0,),

                          Flexible(
                            child: Column(
                              children: [
                                //region NAME
                                Focus(
                                  child: ProfileTextFormField(
                                    controller: _name,
                                    icon: Icons.person,
                                    hintText: 'Name',
                                  ),
                                  onFocusChange: (hasFocus) async {
                                    if (!hasFocus) {
                                      await db.updateUser(name: _name.text);
                                    }
                                  },
                                ),
                                //endregion
                                const SizedBox(height: 10.0,),
                                //region USERNAME
                                Focus(
                                  child: ProfileTextFormField(
                                    controller: _username,
                                    icon: Icons.alternate_email,
                                    hintText: 'Username',
                                    onChanged: (value) => _isUsernameValid = true,
                                    validator: (value) {
                                      if (value == null || value.length < 3) {
                                        return 'Username must be 3+ characters long';
                                      }
                                      else {
                                        return _isUsernameValid ? null : 'Username already taken';
                                      }
                                    },
                                  ),
                                  onFocusChange: (hasFocus) async {
                                    // check if the chosen username is already taken
                                    // and update a variable that will be used by the field validator
                                    if (!hasFocus && _formKey.currentState!.validate()) {
                                      List<String> existingUsernames = await db.getAllUsernames();
                                      if (_username.text == currUser.username) {
                                        setState(() => _isUsernameValid = true);
                                      }
                                      else if (existingUsernames.contains(_username.text)) {
                                        setState(() => _isUsernameValid = false);
                                        _formKey.currentState!.validate();
                                      }
                                      else {
                                        db.updateUser(username: _username.text);
                                      }
                                    }
                                  },
                                ),
                                //endregion
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 25.0,),
                      //endregion

                      //region BIRTHDAY
                      ProfileDateTimeFormField(
                        initialValue: _birthday,
                        lastDate: DateTime.now(),
                        hintText: 'Birthday',
                        icon: Icons.cake,
                        onDateSelected: (value) async {
                          setState(() => _birthday = value);
                          await db.updateUser(birthday: _birthday.toString());
                        },
                      ),
                      const SizedBox(height: 50.0,),
                      //endregion

                      //region ABOUT
                      Focus(
                        child: ProfileTextAreaFormField(
                          controller: _about,
                          label: 'About',

                        ),
                        onFocusChange: (hasFocus) {
                          if(!hasFocus) {
                            db.updateUser(about: _about.text);
                          }
                        },
                      ),
                      const SizedBox(height: 50.0,),
                      //endregion

                      //region SHARE BUTTON
                      Builder(
                        builder: (context) {
                          // required for the sharePositionOrigin needed to share from iPads
                          RenderBox? box;
                          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                            box = context.findRenderObject() as RenderBox?;
                          });

                          return IconButton(
                            iconSize: 48.0,
                            padding: const EdgeInsets.only(right: 5.0),
                            onPressed: () async {
                              Rect? sharePos;
                              if (box != null) {
                                sharePos = box!.localToGlobal(Offset.zero) & box!.size;
                              }

                              await Share.share(
                                Constants.sharingMessage(currUser, currUser: true),
                                sharePositionOrigin: sharePos,
                              );
                            },
                            icon: Icon(
                              Icons.share,
                              color: Theme.of(context).colorScheme.onBackground,
                            ),
                          );
                        }
                      ),
                      //endregion
                    ],
                  ),
                );
              }
          ),
        ),
      ),
    );
  }
}

class LandscapeProfile extends StatefulWidget {
  const LandscapeProfile({Key? key, required this.auth}) : super(key: key);

  final AuthService auth;

  @override
  State<LandscapeProfile> createState() => _LandscapeProfileState();
}
class _LandscapeProfileState extends State<LandscapeProfile> {

  final GlobalKey<FormState> _column1Key = GlobalKey<FormState>();
  final GlobalKey<FormState> _column2Key = GlobalKey<FormState>();

  late final TextEditingController _name;
  late final TextEditingController _username;
  DateTime? _birthday;
  late final TextEditingController _about;

  bool _isUsernameValid = true;

  @override
  void initState() {
    super.initState();
    _name = TextEditingController(text: widget.auth.currUser!.name);
    _username = TextEditingController(text: widget.auth.currUser!.username);
    _birthday = widget.auth.currUser!.birthday;
    _about = TextEditingController(text: widget.auth.currUser!.about);
  }

  @override
  Widget build(BuildContext context) {

    MyUser currUser = widget.auth.currUser!;

    return Row(
      children: [
        //region NAVBAR
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              if (constraints.maxHeight <= 500) {
                return vertNavBar(context, 2);
              }
              else {
                return SizedBox(height: 500, child: vertNavBar(context, 2),);
              }
            },
          ),
        ),
        //endregion

        //region COLUMN 1
        Flexible(
          flex: 5,
          child: SingleChildScrollView(
            child: Form(
              key: _column1Key,
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: Column(
                  children: [
                    //region PROFILE PIC
                    Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return GestureDetector(
                          onTapUp: (details) async {
                            await showProfilePicMenu(
                              context: context,
                              dbService: db,
                              offset: details.globalPosition,
                              currProfilePic: currUser.profilePic,
                            );
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15.0),
                            child: Container(
                              width: MediaQuery.of(context).size.height / 4,
                              height: MediaQuery.of(context).size.height / 4,
                              color: Theme.of(context).colorScheme.background,
                              child: currUser.profilePic ?? const RandomColoredBox(),
                            ),
                          ),
                        );
                        }
                    ),
                    //endregion

                    //region NAME FIELD
                    Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 35,
                            vertical: MediaQuery.of(context).size.height / 35,
                          ),
                          child: Focus(
                            child: ProfileTextFormField(
                              controller: _name,
                              icon: Icons.person,
                              hintText: 'Name',
                            ),
                            onFocusChange: (hasFocus) async {
                              if (!hasFocus) {
                                await db.updateUser(name: _name.text);
                              }
                            },
                          ),
                        );
                      }
                    ),
                    //endregion

                    //region USERNAME FIELD
                    Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 35,
                          ),
                          child: Focus(
                            child: ProfileTextFormField(
                              controller: _username,
                              icon: Icons.alternate_email,
                              hintText: 'Username',
                              onChanged: (value) => _isUsernameValid = true,
                              validator: (value) {
                                if (value == null || value.length < 3) {
                                  return 'Username must be 3+ characters long';
                                }
                                else {
                                  return _isUsernameValid ? null : 'Username already taken';
                                }
                              },
                            ),
                            onFocusChange: (hasFocus) async {
                              // check if the chosen username is already taken
                              // and update a variable that will be used by the field validator
                              if (!hasFocus && _column1Key.currentState!.validate()) {
                                List<String> existingUsernames = await db.getAllUsernames();
                                if (_username.text == currUser.username) {
                                  setState(() => _isUsernameValid = true);
                                }
                                else if (existingUsernames.contains(_username.text)) {
                                  setState(() => _isUsernameValid = false);
                                  _column1Key.currentState!.validate();
                                }
                                else {
                                  db.updateUser(username: _username.text);
                                }
                              }
                            },
                          ),
                        );
                      }
                    ),
                    //endregion
                  ],
                ),
              ),
            ),
          ),
        ),
        //endregion

        //region SHARE BUTTON
        Flexible(
          flex: 1,
          child: Builder(
            builder: (context) {
              // required for the sharePositionOrigin needed to share from iPads
              RenderBox? box;
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                box = context.findRenderObject() as RenderBox?;
              });

              return IconButton(
                iconSize: 48.0,
                padding: const EdgeInsets.only(right: 5.0),
                onPressed: () async {
                  Rect? sharePos;
                  if (box != null) {
                    sharePos = box!.localToGlobal(Offset.zero) & box!.size;
                  }

                  await Share.share(
                    Constants.sharingMessage(currUser, currUser: true),
                    sharePositionOrigin: sharePos,
                  );
                },
                icon: Icon(
                  Icons.share,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              );
            }
          ),
        ),
        //endregion

        Flexible(
          flex: 5,
          child: SingleChildScrollView(
            child: Form(
              key: _column2Key,
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: Column(
                  children: [
                    //region BIRTHDAY
                    Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 35,
                            vertical: MediaQuery.of(context).size.height / 35,
                          ),
                          child: ProfileDateTimeFormField(
                            initialValue: _birthday,
                            lastDate: DateTime.now(),
                            hintText: 'Birthday',
                            icon: Icons.cake,
                            onDateSelected: (value) async {
                              setState(() => _birthday = value);
                              await db.updateUser(birthday: _birthday.toString());
                            },
                          ),
                        );
                      }
                    ),
                    //endregion

                    //region ABOUT
                    Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 35,
                          ),
                          child: Focus(
                            child: ProfileTextAreaFormField(
                              controller: _about,
                              label: 'About',

                            ),
                            onFocusChange: (hasFocus) {
                              if(!hasFocus) {
                                db.updateUser(about: _about.text);
                              }
                            },
                          ),
                        );
                      }
                    ),
                    //endregion
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
