/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/services/theme.dart';
import 'package:presents/utils/default_snackbar.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:presents/utils/theme_switcher.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsDialog extends StatefulWidget {
  const SettingsDialog({Key? key, required this.auth, required this.db}) : super(key: key);

  final AuthService auth;
  final DatabaseService db;

  @override
  State<SettingsDialog> createState() => _SettingsDialogState();
}

class _SettingsDialogState extends State<SettingsDialog> {

  String? nightModeText;
  bool showRestartNeededNote = false;

  String getNewText(ThemeMode themeMode) {
    switch (themeMode) {
      case ThemeMode.system:
        return 'AUTO';
      case ThemeMode.light:
        return 'OFF';
      case ThemeMode.dark:
        return 'ON';
    }
  }

  void _updateNightModeText(ThemeMode newThemeMode) {
    setState(() {
      nightModeText = getNewText(newThemeMode);
      showRestartNeededNote = true;
    });
  }

  @override
  Widget build(BuildContext context) {

    nightModeText ??= getNewText(Provider.of<ThemeManager>(context, listen: false).currThemeMode!);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          //region Dark Mode Switcher
          Consumer<ThemeManager>(
              builder: (context, tM, child) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ThemeSwitcher(
                      iconLeft: Icons.light_mode_rounded,
                      iconMiddle: Icons.brightness_6_rounded,
                      iconRight: Icons.dark_mode_rounded,
                      themeManager: tM,
                      updateNightModeText: _updateNightModeText,
                    ),
                    const SizedBox(width: 10.0,),
                    Text(
                      'Night Mode: $nightModeText',
                      style: const TextStyle(fontSize: 14.0),
                    ),
                  ],
                );
              }
          ),
          const SizedBox(height: 5.0,),
          Visibility(
            visible: showRestartNeededNote,
            child: const Text(
              'Night Mode preferences need an app restart to take effect',
              style: TextStyle(fontStyle: FontStyle.italic, fontSize: 14.0),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20.0,),
          //endregion

          //region CURRENCY SELECTOR
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Currency: '),
              const SizedBox(width: 10.0,),
              DropdownButton<String>(
                borderRadius: BorderRadius.circular(10.0),

                value: widget.auth.currUser?.currency,
                items: const [
                  DropdownMenuItem(
                    value: '€',
                    child: Text('€')
                  ),
                  DropdownMenuItem(
                    value: '\$',
                    child: Text('\$'),
                  ),
                  DropdownMenuItem(
                    value: '£',
                    child: Text('£'),
                  ),
                  DropdownMenuItem(
                    value: '₹',
                    child: Text('₹'),
                  ),
                  DropdownMenuItem(
                    value: '¥',
                    child: Text('¥'),
                  ),
                  DropdownMenuItem(
                    value: '₽',
                    child: Text('₽'),
                  ),
                ],
                onChanged: (value) => widget.db.updateUser(currency: value),
              ),
            ],
          ),
          const SizedBox(height: 20.0,),
          //endregion

          //region SUPPORT BUTTON
          TextButton.icon(
            onPressed: () async {
              Uri kofiUrl = Uri.parse('https://ko-fi.com/lezsoft');
              if (await canLaunchUrl(kofiUrl)) {
                await launchUrl(kofiUrl, mode: LaunchMode.externalApplication);
              }
            },
            icon: ImageIcon(
              const AssetImage('assets/images/ko-fi.png'),
              color: Theme.of(context).colorScheme.onBackground,
              size: 36.0,
            ),
            label: Text(
              'Support us with a coffee',
              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
            ),
          ),
          const SizedBox(height: 20.0,),
          //endregion

          //region LogOut Button
          IconButton(
            icon: Icon(Icons.exit_to_app, size: 32, color: Theme.of(context).colorScheme.onTertiary,),
            onPressed: () => widget.auth.logOut().then((value) {
              if (value == null) {
                context.goNamed(routeName(RouteCodes.login));
              }
              else {
                ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar(value));
              }
            }),
          ),
          const Text('Log Out'),
          //endregion
        ],
      ),
    );
  }
}
