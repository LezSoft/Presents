/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:presents/utils/default_appbar.dart';

/// Customized AppBar for the Search Screen
/// (the appbar will act also as a search field after the search button is tapped)
class SearchAppBar extends StatefulWidget {
  const SearchAppBar({
    Key? key, required this.updateParentSearchState, required this.focusNode}) : super(key: key);

  final Function updateParentSearchState;
  final FocusNode focusNode;

  @override
  State<SearchAppBar> createState() => _SearchAppBarState();
}

class _SearchAppBarState extends State<SearchAppBar> {

  final GlobalKey _formKey = GlobalKey<FormState>();
  bool _isSearching = false;

  Timer? _searchCooldown;
  final TextEditingController _searchedText = TextEditingController(text: '');

  void _updateSearchState({bool clearSearchText = false, bool exitSearchMode = false}) {
    // Stop the search cooldown if already running
    if (_searchCooldown != null && _searchCooldown!.isActive) {
      _searchCooldown!.cancel();
    }

    // If the "clear" button has been pressed, immediately update the parent
    // searchState with an empty string
    if (clearSearchText) {
      widget.updateParentSearchState('');
    }
    // Else if the "back" button has been pressed, immediately update the parent
    // searchState to exit Search Mode
    else if (exitSearchMode) {
      widget.updateParentSearchState(null);
    }
    // Else, start a searchCooldown that will update the parent searchState after
    // 0.5 second of non-typing
    else {
      _searchCooldown = Timer(const Duration(milliseconds: 500), () {
        widget.updateParentSearchState(_searchedText.text);
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        InkWell(
            onTap: () {
              widget.updateParentSearchState(_searchedText.text);
              setState(() => _isSearching = true);
            },
            child: DefaultAppBar(
              title: 'Search',
              actions: [
                IconButton(
                    onPressed: () {
                      widget.updateParentSearchState(_searchedText.text);
                      setState(() => _isSearching = true);
                    },
                    icon: const Icon(Icons.search)
                ),
              ],
            )
        ),
        if (_isSearching)
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: AppBar(
              backgroundColor: themeColors.primary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              leading: IconButton(
                // padding: const EdgeInsets.all(18.5),
                icon: Icon(Icons.arrow_back, color: themeColors.onPrimary,),
                onPressed: () {
                  setState(() => _isSearching = false);
                  _updateSearchState(exitSearchMode: true);
                },
              ),
              titleSpacing: 5.0,
              title: Form(
                key: _formKey,
                child: TextFormField(
                  controller: _searchedText,
                  focusNode: widget.focusNode,
                  autofocus: true,
                  style: TextStyle(color: themeColors.onPrimary, fontSize: 19.0),
                  cursorColor: themeColors.onPrimary,
                  onChanged: (value) {
                    _updateSearchState();
                  },
                  decoration: InputDecoration(
                    fillColor: themeColors.primary,
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeColors.primary)
                    ),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeColors.primary)
                    ),
                    hintText: "your friend's username...",
                    suffixIcon: (_searchedText.text != '') ? IconButton(
                        color: themeColors.onPrimary,
                        icon: const Icon(Icons.clear),
                        onPressed: () {
                          _searchedText.clear();
                          _updateSearchState(clearSearchText: true);
                        }
                    ) : null,
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }
}