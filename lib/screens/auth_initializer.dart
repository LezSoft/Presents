/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/loading.dart';
import 'package:provider/provider.dart';

class AuthServiceInitializer extends StatelessWidget {
  AuthServiceInitializer({Key? key, required String passedOriginalDest}) :
    originalDest = passedOriginalDest.replaceAll('__', '/'),
    super(key: key);

  final String originalDest;

  @override
  Widget build(BuildContext context) {
    AuthService auth = Provider.of<AuthService>(context);

    return FutureBuilder(
      future: auth.initialize(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            context.go(originalDest);
          });
        }
        return const Loading();
      }
    );
  }
}
