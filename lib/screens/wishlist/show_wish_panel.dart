/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/present.dart';
import 'package:presents/screens/wishlist/wish_form.dart';
import 'package:presents/services/database.dart';

/// A method to show a modal bottom sheet containing the WishForm
showNewWishPanel(BuildContext context, DatabaseService db, {Present? present, String? newPresentLink}) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
              child: WishForm(db: db, present: present, newPresentLink: newPresentLink,),
            ),
          ),
        );
      }
  );
}