/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

enum RouteCodes {
  wishlist,
  search,
  otherWishlist,
  otherProfile,
  profile,
  login,
  signup,
  pswReset,
  authInit,
}

String routeName(RouteCodes code) => code.toString();

String routePath(RouteCodes code, {bool forRouteDeclaration = true}) {
  switch (code) {
    case RouteCodes.wishlist:
      return '/';
    case RouteCodes.search:
      return '/search';
    case RouteCodes.otherWishlist:
      return forRouteDeclaration ? 'wishlist/:uid' : '/search/wishlist';
    case RouteCodes.otherProfile:
      return forRouteDeclaration ? 'profile/:uid' : '/search/profile';
    case RouteCodes.profile:
      return '/profile';
    case RouteCodes.login:
      return '/login';
    case RouteCodes.signup:
      return '/signup';
    case RouteCodes.pswReset:
      return '/password-reset';
    case RouteCodes.authInit:
      return forRouteDeclaration ? '/loading/:next' : '/loading';
  }
}

RouteCodes? getCodeFromPath(String path) {

  for (RouteCodes code in RouteCodes.values) {
    String codePathForDeclaration = routePath(code);
    String codePath = routePath(code, forRouteDeclaration: false);

    // for all the "traditional" routes check if path is the same as the one returned by routePath
    if (codePathForDeclaration == codePath) {
      if (path == codePath) {
        return code;
      }
    }
    // for "special" ones (the ones with parameters) use .contains instead of == (to ignore parameters)
    else {
      if (path.contains(codePath)) {
        return code;
      }
    }
  }

  // if arrived here, no code has matched, so return null
  return null;
}