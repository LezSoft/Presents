/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import 'package:flutter/material.dart';

SnackBar defaultSnackBar(String message, {bool error = true}) {
  return SnackBar(
    content: Row(
      children: [
        Icon(
          error ? Icons.error_sharp : Icons.thumb_up,
          color: Colors.white,
        ),
        const SizedBox(width: 20.0,),
        Expanded(
          child: Text(
            message,
            style: const TextStyle(
              color: Colors.white,
              fontFamily: 'Poppins'
            ),
          ),
        ),
      ],
    ),
    backgroundColor: error ? Colors.red.withOpacity(0.75) : Colors.green.withOpacity(0.75),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    ),
    behavior: SnackBarBehavior.floating,
    duration: const Duration(seconds: 5),
  );
}