/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:presents/models/my_user.dart';
import 'package:presents/services/auth.dart';

/// Class used to standardize parameters passed to routes
class UrlParams {
  // Constructor
  UrlParams({required this.authService, required this.otherUser});

  final AuthService authService;
  final MyUser otherUser;
}