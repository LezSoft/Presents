#  This file is part of Presents.
#  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
#  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
#  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
#
#  SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
#  SPDX-License-Identifier: GPL-3.0-or-later

import base64
import requests
from appwrite.client import Client
from bs4 import BeautifulSoup

"""
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
"""


def amazon_fetcher(soup):
    # Try to extract the <img> element (works for most of Amazon Product Pages)
    image_elem = soup.find("img", id="landingImage")
    if (image_elem != None):
        return image_elem['src']
    # if no matching <img> element was found (for example with Book pages), manually scraping the page html (as a string)
    # to find the part containing {"landingImageUrl":"<ACTUAL_URL>"}
    else:
        soup_str = str(soup)
        url_start_index = soup_str.find("landingImageUrl")
        soup_str = soup_str[url_start_index:]
        url_end_index = soup_str.find("}")
        soup_str = soup_str[18:url_end_index-1]
        return soup_str


def unieuro_fetcher(soup):
    return soup.find("div", class_="stickyCta__info-img").find("img")['src']


problematic_urls = {
    '//www.amazon': amazon_fetcher,
    '//amazon': amazon_fetcher,
    '//amzn': amazon_fetcher,
    'unieuro.it': unieuro_fetcher,
}


def main(req, res):
    client = Client()

    # Check if the request provided all reqired env variables
    # and initialize the client var
    if not req.variables.get('APPWRITE_FUNCTION_ENDPOINT') or not req.variables.get('APPWRITE_FUNCTION_API_KEY'):
        print('Environment variables are not set. Function cannot use Appwrite SDK.')
    else:
        (
            client
            .set_endpoint(req.variables.get('APPWRITE_FUNCTION_ENDPOINT', None))
            .set_project(req.variables.get('APPWRITE_FUNCTION_PROJECT_ID', None))
            .set_key(req.variables.get('APPWRITE_FUNCTION_API_KEY', None))
        )

    # region FIELDS

    # The headers that will be used to make requests
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    }
    # the url passed when the function has been called
    raw_url: str = req.payload
    # the url that will be used to download the image
    image_url: str = ''
    # the bytes of the downloaded image
    image_bytes: bytes
    # the downloaded image encoded with base64 as a String
    encoded_image = None

    # endregion

    if raw_url != '':
        # region GET THE image_url

        # Get the product page HTML as a BeautifulSoup object
        productPageReq = requests.get(raw_url, headers=headers)
        productPageSoup = BeautifulSoup(productPageReq.content, 'html.parser')

        # If the raw_url is one of the known problematic ones, fetch the image_url using its specific function
        for key in problematic_urls.keys():
            if key in raw_url:
                image_url = problematic_urls[key](productPageSoup)
                break

        # else (if the image_url is still empty), fetch it using metadata tags
        if image_url == '':
            image_url = productPageSoup.find("meta", property="og:image")["content"]
            # and add https in front of the url if needed
            # (some websites have their image urls in meta tags that start just with '//')
            if image_url.startswith('//'):
                image_url = f'https:{image_url}'

        # endregion

        if image_url != '':
            # Download the image as Bytes
            image_bytes = requests.get(image_url, headers=headers).content

            if image_bytes:
                # Encode the just downloaded image it using base64 (a.k.a. convert it into a string)
                encoded_image = base64.b64encode(image_bytes)

    if encoded_image is not None:
        res.json(
            {
                "passed_url": raw_url,
                "image_url": image_url,
                "encoded_image": encoded_image.decode('ascii')
            },
            200
        )
    elif image_url:
        res.send('Something went wrong downloading the image', 500)
    else:
        res.send('Unable to fetch the image url from the given link', 500)
