Presents is the ultimate social-wishlist app that you didn't know you needed.

Why? Because with that one single app you will solve 3 huge problems:
1. Keeping losing the papers where you had written down the super cool product you saw and definitely wanted
2. Not knowing what to gift to someone, but preferring to avoid the pathetic question: “hey, what do you want as a gift?”
3. Having to think a different answer for each time someone asks you the pathetic question above

How, you ask?
On Presents you can keep your wishes organized in one place, and synced across all your devices.
Time to think of a gift for one of your friend? Simply search its username in the Search tab and take inspiration from its own wishlist!

Intrigued, aren't you?
Visit https://lezsoft.com/presents to discover more features or just download the app and give it a try!